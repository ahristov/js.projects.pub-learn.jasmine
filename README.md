Project Layout for TDD/BDD with Jasmine
=======================================

This is a layout for TDD/BDD project with Jasmine. The tests can be run in three different environments:


- In browser tests

The test suite is an `html file` which you just open in your browser.

This type of testing looks to be good when the code you are developing is heavily UI related.

It also is easy to run and does not need setup.



- jsTestDriver testing

The `jsTestDriver` is suitable in case your goal is to make sure the code
you are writing will work in different web browsers.

It needs some preparation - installing `java`, capturing web browsers, etc.



- node.js testing

If the code you are testing does not depend on web browser, just run node.js tests from the command line.

Keep in mind, the JavaScript implementation in the different web browsers may behave differently even for code not related to UI!
For that reason you should use the jsTestDriver testing to double check your code.

We'll have to have `node.js` installed on your computer.


## Project structure

The project has the following structure:

	README.md			this file
	code				the SUT (system under test)
	code\deps			if the SUT has some dependencies (jQuery, underscore, etc.)
	code\src			the source code of the SUT
	reports				(opt) eventually created from the test runner
	test				the tests
	test\deps			if the tests have some dependencies (Jasmine, etc.)
	test\node_modules	node modules used in the node.js testing
	test\plugins		plugins for jsTestDriver
	test\src			the actual tests code
	node_modules		contains node modules relevant for the project
	reports				the jUnit compatable reports after running the tests
	run-jstd_server.bat	runs jsTestDriver in server mode
	run-jstd_tests.bat	runs jsTestDriver in client mode (runs tests)
	run-node_tests.bat	runs node.js tests

Please note the spec files for Jasmine should be named like '*.spec.js'



## Runing the tests

### In Browser tests

Open the 'test/run.html' in your browser

### jsTestDriver testing

1. Run the BAT file `run-jstd_tests.bat`:

2. Open the following page in your browser:
> http://localhost:42442/capture/

3. To run the tests run the BAT file `run-jstd_tests.bat`

### node.js tests

While all the other testing methods involves a browser to run them,
the node.js tests can be run from the command line:

	C:\Projects\js.projects.pub-learn.jasmine>node test\node_modules\jasmine\bin\jasmine-node test
	..

	Finished in 0.006 seconds
	2 tests, 3 assertions, 0 failures


## Git dependencies to node modules

For the node.js testing the following dependencies are added:

	git submodule add git://github.com/mhevery/jasmine-node.git test/node_modules/jasmine
	git submodule add git://github.com/larrymyers/jasmine-reporters.git test/node_modules/jasmine-reporters
	git submodule add git://github.com/substack/node-findit.git test/node_modules/findit
	git submodule add git://github.com/substack/node-seq.git test/node_modules/seq
	git submodule add git://github.com/substack/node-hashish.git test/node_modules/hashish
	git submodule add git://github.com/substack/js-traverse.git test/node_modules/traverse
	git submodule add https://github.com/substack/node-chainsaw.git test/node_modules/chainsaw
	git submodule add  git://github.com/soldair/node-walkdir.git
 test/node_modules/chainsaw

When cloning the repository, <b>run the following</b> from project's top directory:

	git submodule init
	git submodule update

### Sidenote: deleting submodules in git

To remove a submodule from git repository:

- Delete the relevant line from the .gitmodules file.
- Delete the relevant section from .git/config.
- Run git rm --cached path_to_submodule (no trailing slash).

Example:

	git rm --cached node_modules/shred

- Commit to git
- Delete the now untracked submodule files.



## Author

atanashristov@hotmail.com (Atanas Hristov)



## Links

[Jasmine](http://pivotal.github.com/jasmine/)

[Jasmine Reporters](https://github.com/larrymyers/jasmine-reporters)

[Jasmine jsTestDriver adapter](https://github.com/ibolmo/jasmine-jstd-adapter)

[jsTestDriver](http://code.google.com/p/js-test-driver/)

[Jasmine for node.js](https://github.com/mhevery/jasmine-node)




