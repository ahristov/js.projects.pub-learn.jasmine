describe("Pet", function() {

	var pet;

	beforeEach(function() {
		pet = new JsWebAppBase.Pet();
	});

	afterEach(function() {
		delete pet;
	});


	it("inherit weight getter", function() {
		expect(pet.getWeight).toBeDefined();
	});

	it("inherit weight setter", function() {
		expect(pet.setWeight).toBeDefined();
	});

	it("has name getter", function() {
		expect(pet.getName).toBeDefined();
	});

	it("has name setter", function() {
		expect(pet.setName).toBeDefined();
	});

	describe("name", function() {
		it("can change", function() {

			pet.setName("Mau");
			expect(pet.getName()).toBe("Mau");

			pet.setName("Miau");
			expect(pet.getName()).not.toBe("Mau");
			expect(pet.getName()).toBe("Miau");
		});
	});


});


