describe("Animal", function() {

	var animal;

	beforeEach(function() {
		animal = new JsWebAppBase.Animal();
	});

	afterEach(function() {
		delete animal;
	});


	it("has weight getter", function() {
		expect(animal.getWeight).toBeDefined();
	});

	it("has weight setter", function() {
		expect(animal.setWeight).toBeDefined();
	});

	describe("weight", function() {
		it("can change", function() {

			animal.setWeight(50);
			expect(animal.getWeight()).toBe(50);

			animal.setWeight(60);
			expect(animal.getWeight()).not.toBe(50);
			expect(animal.getWeight()).toBe(60);
		});
	});


});


