/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 4/14/12
 * Time: 8:12 PM
 *
 * Contains definition of class Cat.
 *
 */


var JsWebAppBase = JsWebAppBase || {};

(function() {

	var Pet = new JsWebAppBase.Class(JsWebAppBase.Animal);

	var name='';

	Pet.include({

		getName: function() {
			return name;
		},

		setName: function(newName) {
			name = newName;
		}

	});


	JsWebAppBase.Pet = Pet;

}());

