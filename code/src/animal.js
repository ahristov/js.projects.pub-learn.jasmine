/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 4/14/12
 * Time: 8:10 PM
 *
 * Contains definition of class Animal.
 */

var JsWebAppBase = JsWebAppBase || {};

(function() {

	var Animal = new JsWebAppBase.Class();

	var weight=0;

	Animal.include({

		canBreath: function() {
			return true;
		},

		getWeight: function() {
			return weight;
		},

		setWeight: function(newWeight) {
			weight = newWeight;
		}

	});


	JsWebAppBase.Animal = Animal;

}());


